﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebPage1.aspx.cs" Inherits="Soln_CrossPostBack_Label.WebPage1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tasl Label</title>
    <link href="Css/label.css" rel="stylesheet" type ="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
    <asp:TextBox ID="txtdata" runat ="server" CssClass="txt" />
        <asp:Button ID="btnsubmit" runat ="server" CssClass ="btn btn2 " Text="Submit" PostBackUrl="~/WebPage2.aspx" />
    </div>
    </form>
</body>
</html>
