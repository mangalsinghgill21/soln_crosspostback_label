﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebPage2.aspx.cs" Inherits="Soln_CrossPostBack_Label.WebPage2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Css/label.css" rel="stylesheet" type ="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <br />
         <div style="text-align: center">
             <asp:Label ID="lbltiny" runat="server" Text="tiny" CssClass="lbltiny" />
        </div>
        <br />
        <div style="text-align: center">
            <asp:Label ID="lblsmall" runat="server" Text="small" CssClass="lblsmall"  />
        </div>
        <br />
        <div style="text-align: center">
            <asp:Label ID="lblmedium" runat="server" Text="medium" CssClass="lblmedium" />
        </div>
        <br />
        <div style="text-align: center">
            <asp:Label ID="lblbig" Text ="big" runat="server" CssClass="lblbig" />
        </div>
        <br />
        <div style="text-align: center">
            <asp:Label ID="lblhuge" runat="server" Text="huge" CssClass="lblhuge" />
        </div>

    </form>
</body>
</html>
