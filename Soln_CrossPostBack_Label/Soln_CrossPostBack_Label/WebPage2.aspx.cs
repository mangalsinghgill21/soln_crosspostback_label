﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_CrossPostBack_Label
{
    public partial class WebPage2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.PreviousPage != null)
            {
                 string strSizes =
                ((TextBox)PreviousPage.FindControl("txtdata")).Text;
             
                lbltiny.Text = strSizes;
                lblsmall.Text = strSizes;
                lblmedium.Text = strSizes;
                lblbig.Text = strSizes;
                lblhuge.Text = strSizes;
            }
        }
    }
}